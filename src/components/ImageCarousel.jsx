import React from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import image1 from '../assets/carousel/01.png';
import image2 from '../assets/carousel/02.png';
import image3 from '../assets/carousel/03.png';
import image4 from '../assets/carousel/04.png';
import image5 from '../assets/carousel/05.png';

const ImageCarousel = () => {
    const settings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000
    };

    return (
        <Slider {...settings}>
            <div>
                <img src={image1} className='h-[72vh] md:h-[85vh]' alt='imgs carousel 1' style={{ width: '100%', objectFit: 'cover' }} />
            </div>
            <div>
                <img src={image2} className='h-[72vh] md:h-[85vh]' alt='imgs carousel 2' style={{ width: '100%', objectFit: 'cover' }} />
            </div>
            <div>
                <img src={image3} className='h-[72vh] md:h-[85vh]' alt='imgs carousel 3' style={{ width: '100%', objectFit: 'cover' }} />
            </div>
            <div>
                <img src={image4} className='h-[72vh] md:h-[85vh]' alt='imgs carousel 4' style={{ width: '100%', objectFit: 'cover' }} />
            </div>
            <div>
                <img src={image5} className='h-[73vh] md:h-[85vh]' alt='imgs carousel 5' style={{ width: '100%', objectFit: 'cover' }} />
            </div>
        </Slider>
    );
};

export default ImageCarousel;
