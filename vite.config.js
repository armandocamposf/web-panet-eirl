import { defineConfig } from "vite";
import react from "@vitejs/plugin-react-swc";
import Unfonts from "unplugin-fonts/vite";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    react(),
    Unfonts({
      custom: {
        display: 'swap',
        families: {
          'Flexo': {
            src: './assets/fonts/Flexo*',
            transform(font) {
              if (font.basename === 'Flexo-Bold')
                font.weight = 700
              return font
            }
          },
        },
      },
    }),
  ],
});
