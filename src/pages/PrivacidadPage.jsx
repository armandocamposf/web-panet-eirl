import React from 'react'
import { FiAlignJustify } from 'react-icons/fi';

export const PrivacidadPage = () => {
  return (
    <div className="flex flex-col pt-10 sm:px-4 md:px-20 ">
      <h1 className='text-4xl ml-5 sm:ml-10 md:text-5xl font-bold text-[#053044] flex flex-row'> <FiAlignJustify style={{ marginRight: 15 }} /> Terminos y Condiciones</h1>
      <span className='text-bold text-[#053044] ml-[4rem] md:ml-[7rem]'>Última actualización el 18 de Abri de 2024</span>

      <p class="text-gray-700 text-base mb-4 indent-8 text-justify px-10 mt-5">
        Bienvenido a Nuestro sitio web y aplicación, que es propiedad de y están operados por PANET EIRL, sociedad constituida bajo las leyes del PERU que opera sus servicios de comunicación en Peru, Chile, Colombia, Brasil, Republica Dominicana, Canada, Ecuador, Mexico Union Europea, Argentina, Costarica y EEUU, respectivamente, a las cuales nos referimos colectivamente como "PANET", "Nosotros", "Nos", o "Nuestro". Este sitio web y esta aplicación forman parte de Nuestra plataforma tecnológica general (en adelante, la "Plataforma") de sitios web y aplicaciones (apps) a través de la cual ofrecemos servicios de comunicación que permiten a dos partes ponerse en contacto y transferir activos digitales entre una y otra.
      </p>

      <p class="text-gray-700 text-base mb-4 indent-8 text-justify px-10 mt-5">
        Nos referimos a los visitantes y usuarios de Nuestra Plataforma de Servicios Tecnológicos como
        “Usted” “Su” o &quot;Sus&quot;. Cuando Usted está registrado y verificado con el fin de utilizar Nuestra
        Plataforma de Servicios Tecnológicos, también Nos referimos a Usted como un &quot;Usuario&quot;.
      </p>
      <p class="text-gray-700 text-base mb-4 indent-8 text-justify px-10 mt-5">
        El uso de Nuestra Plataforma de Servicios Tecnológicos se proporciona únicamente bajo los términos
        y condiciones de este Contrato y las leyes aplicables al país en el que haya tenido lugar la transacción
        y en el que PANET E.I.R.L opera sus servicios.
      </p>
      <p class="text-gray-700 text-base mb-4 indent-8 text-justify px-10 mt-5">
        Al aceptar los términos de este Contrato, Usted acepta que usamos cookies para rastrear el uso del
        sitio web, lo que Nos permite comprender mejor las necesidades de Nuestros Usuarios y
        proporcionar experiencias en el sitio web dirigidas a los intereses potenciales de los Usuarios.
      </p>
      <p class="text-gray-700 text-base mb-4 indent-8 text-justify px-10 mt-5">
        La plataforma/app y/o servicios proporcionados por Retorna no son actividades autorizadas
        restrictivamente a entidades financieras de ninguna forma, ni tienen por objeto promover o
        publicitar servicios financieros a residentes de los países antes mencionado o dentro del territorio
        nacional. PANET en ningún momento capta masivamente dinero del público bajo los términos de las
        leyes de Peru, Chile, Colombia, Brasil, Republica Dominicana, Canada, Ecuador, Mexico, Union
        Europea, Argentina, Costarica y EEUU ni tampoco proporciona servicios con el objeto de invertir
        dinero de los usuarios en ningún tipo de activo. Panet bajo ningún concepto ofrece servicios que
        generen rentabilidad o Ganancias derivados de inversión a sus usuarios. Panet maneja una cantidad
        de activos digitales sobre los que respalda la comunicación de órdenes de canje por divisas entre un
        emisor y receptor.
      </p>
      <p class="text-gray-700 text-base mb-4 indent-8 text-justify px-10 mt-5">
        Notifique a PANET inmediatamente por correo electrónico a *contacto@paneteirl.com* si cree que
        su nombre de usuario, contraseña o la información de Recarga Whalet ha sido suplantada o utilizada
        sin su consentimiento o autorización.
      </p>
    </div>
  )
}
