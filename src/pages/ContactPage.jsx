import React from 'react'
import { FiHome, FiPhone, FiBriefcase, FiUsers, FiMail } from 'react-icons/fi';
import { CustomHR } from '../components/CustomHR';

export const ContactPage = () => {
  return (
    <div className="flex flex-col pt-10 sm:px-4 md:px-20 ">
      <h1 className='text-4xl ml-5 sm:ml-10 md:text-5xl font-bold text-[#053044] flex flex-row'> <FiPhone style={{ marginRight: 15 }} /> Contactanos</h1>
      <CustomHR className="ml-5 " width={'30vw'} />

      <div className="flex flex-col sm:flex-row">
        {/* Teléfono */}
        <div className="w-full sm:w-1/3 p-4 flex flex-col items-center justify-center">
          <div className="text-5xl p-3 rounded-2xl text-white bg-[#053044]">
            <FiPhone />
          </div>
          <div className="text-3xl text-center p-3 text-[#053044]">
            Teléfono <br />
            +51910632501 <br />
            +51916902417
          </div>
        </div>

        {/* Correo */}
        <div className="w-full sm:w-1/3 p-4 flex flex-col items-center justify-center">
          <div className="text-5xl p-3 rounded-2xl text-white bg-[#053044]">
            <FiMail />
          </div>
          <div className="text-3xl text-center p-3 text-[#053044]">
            Correo <br />
            contacto@paneteirl.com
          </div>
        </div>

        {/* Dirección */}
        <div className="w-full sm:w-1/3 p-4 flex flex-col items-center justify-center">
          <div className="text-5xl p-3 rounded-2xl text-white bg-[#053044]">
            <FiHome />
          </div>
          <div className="text-3xl text-center p-3 text-[#053044]">
            Dirección <br />
            AV PERU 3013 SMP LIMA PERÚ
          </div>
        </div>
      </div>
    </div>
  )
}
