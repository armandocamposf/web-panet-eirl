import React from 'react'

export const CustomHR = ({width}) => {
    const hrStyle = {
        width: width,   // Ancho de la línea
        height: '6px',  // Grosor de la línea
        backgroundColor: '#053044',  // Color de fondo de la línea
        border: 'none',  // Sin borde para eliminar el borde predeterminado
        marginTop: 10
    };

    return (
        <hr style={hrStyle} />
    );
}
