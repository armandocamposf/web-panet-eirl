import React from 'react';
import backgroundImage from '../assets/header.png';
import leftImage from '../assets/logo.png'
import DropdownMenu from './DropdownMenu';
import { Link } from 'react-router-dom';
import Navigation from './Navigation';

export const Header = () => {
    return (
        <header
            className="fixed top-0 left-0 w-full bg-cover h-[28vh] md:h-[15vh]"
            style={{ backgroundImage: `url(${backgroundImage})`, }}
        >
            <nav className="flex flex-col md:flex-row md:justify-between items-center h-full px-4 ">
                <div className="flex items-center">
                    <Link to="/">
                        <img src={leftImage} alt="LOGO PRINCIPAL" className=' w-[80vw] md:w-[25vw]' />
                    </Link>
                </div>

                <div className="flex flex-col items-end mt-4 md:mt-0">
                    <section className="flex flex-row">
                        <input
                            type="text"
                            className="rounded-2xl pr-4 mr-2 bg-gray-100 text-lg p-1 text-right placeholder-[#053044] placeholer-text-bold text-bold w-[80vw] md:w-[25vw]"
                            placeholder="TRACKING"
                        />
                        <DropdownMenu />
                    </section>
                    <footer>
                        <Navigation />
                    </footer>
                </div>
            </nav>
        </header>
    );
}
