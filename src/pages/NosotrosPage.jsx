import React from 'react'
import { FiInfo } from 'react-icons/fi';

export const NosotrosPage = () => {
  return (
    <div>
      <div className="flex justify-center items-center bg-gray-200" style={{ marginTop: '15vh', height: '85vh' }}>
        {/* Cuadro grande con descripción */}
        <div className="bg-white w-2/3 p-8 rounded-lg shadow-lg relative">
          <div className="bg-gray-800 w-24 h-24 rounded-full flex items-center justify-center absolute left-4">
            <FiInfo className="text-white text-3xl" />
          </div>

          <h2 className="text-2xl font-bold mb-4">Descripción</h2>
          <p className="text-lg text-gray-700">
            Panet EIRL es una empresa especializada en servicios integrales de importación y exportación...
          </p>

        </div>

        {/* Cuadro pequeño con icono superpuesto */}

      </div>

    </div>
  )
}
