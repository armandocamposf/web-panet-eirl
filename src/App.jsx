import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom'; // Importa BrowserRouter as Router
import { MyRoutes } from './routers/routes';
import { Header } from './components/Header';
import { Analytics } from "@vercel/analytics/react";
import { SpeedInsights } from "@vercel/speed-insights/react";



function App() {
  return (
    <Router>
        <SpeedInsights />
        <Analytics />
        <Header />
        <div className="h-[72vh] mt-[28vh] md:h-[85vh] md:mt-[15vh]">
          <MyRoutes />
        </div>
    </Router>
  );
}

export default App;
