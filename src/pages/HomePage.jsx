import React from 'react'
import ImageCarousel from '../components/ImageCarousel'

export const HomePage = () => {
    return (
        <div className='h-[72vh] mt-[28vh] md:h-[85vh] md:mt-[15vh]'>
            <ImageCarousel />
        </div>
    )
}
