import React from 'react';
import { Link, useNavigate } from "react-router-dom";
import { FiHome, FiPhone, FiLock, FiUsers } from 'react-icons/fi';
function Navigation() {
    return (
        <nav className='ml-9 md:ml-0' style={{ marginTop: 5 }}>
            <ul className='flex flex-wrap' style={{ display: 'flex', listStyle: 'none', padding: 0 }}>
                <Link to="/">
                    <li className='buttonNavigatio' style={{ marginRight: '10px' }}>
                        Inicio <FiHome className='iconNavigation' />
                    </li>
                </Link>
                <Link to="/contacto">
                    <li className='buttonNavigatio' style={{ marginRight: '10px' }}>
                        Contacto <FiPhone className='iconNavigation' />
                    </li>
                </Link>
                <Link to="/nosotros">
                    <li className='buttonNavigatio' style={{ marginRight: '10px' }}>
                        Nosotros <FiUsers className='iconNavigation' />
                    </li>
                </Link>
                <Link to="/politicas-privacidad">
                    <li className='buttonNavigatio' style={{ marginRight: '10px' }}>
                        Privacidad <FiLock className='iconNavigation' />
                    </li>
                </Link>
            </ul>
        </nav>
    );
}

export default Navigation;