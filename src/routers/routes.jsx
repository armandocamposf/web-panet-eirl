import { Routes, Route } from "react-router-dom";
import React from 'react'
import { HomePage } from "../pages/HomePage";
import { ContactPage } from "../pages/ContactPage";
import { NosotrosPage } from "../pages/NosotrosPage";
import { PrivacidadPage } from "../pages/PrivacidadPage";
export const MyRoutes = () => {
  return (
        <Routes >
          <Route path="/" exact element={<HomePage />} />
          <Route path="/contacto" exact element={<ContactPage />} />
          <Route path="/nosotros" exact element={<NosotrosPage />} />
          <Route path="/politicas-privacidad" exact element={<PrivacidadPage />} />
        </Routes>
  )
}
