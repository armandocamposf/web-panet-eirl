import React, { useState } from 'react';
import { FiMoreVertical } from 'react-icons/fi';

const DropdownMenu = () => {
    const [isOpen, setIsOpen] = useState(false);

    const toggleDropdown = () => {
        setIsOpen(!isOpen);
    };

    const handleOptionClick = (option) => {
        console.log(`Opción seleccionada: ${option}`);
        setIsOpen(false);
    };

    return (
        <div className="relative inline-block text-left">
            <div>
                <button
                    name="selector"
                    type="button"
                    className="text-gray-600 hover:text-gray-900 focus:outline-none"
                    onClick={toggleDropdown}
                >
                    <FiMoreVertical className="w-9 h-9 mt-1 text-white" />
                </button>
            </div>

            {/* Dropdown */}
            {isOpen && (
                <div className="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5">
                    <div className="py-1" role="menu" aria-orientation="vertical" aria-labelledby="options-menu">
                        <button
                        name="opcion1"
                            onClick={() => handleOptionClick('Option 1')}
                            className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 w-full text-left"
                            role="menuitem"
                        >
                            Paqueteria
                        </button>
                        <button
                        name="opcion2"
                            onClick={() => handleOptionClick('Option 2')}
                            className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 w-full text-left"
                            role="menuitem"
                        >
                            Remesa
                        </button>
                    </div>
                </div>
            )}
        </div>
    );
};

export default DropdownMenu;
